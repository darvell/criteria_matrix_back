CREATE TABLE test_schedule
(
    id            INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    test_id       INTEGER             NOT NULL,
    user_id       INTEGER             NOT NULL,
    join_code     VARCHAR(255),
    create_date   DATETIME            NOT NULL,
    complete_date DATETIME,
    deleted       TINYINT default 0,

    CONSTRAINT test_schedule_test_fk FOREIGN KEY (test_id) REFERENCES test (id)
);

ALTER TABLE test_process
    ADD COLUMN test_schedule_id INTEGER;

ALTER TABLE test_process
    MODIFY COLUMN test_id INTEGER;

ALTER TABLE test_process
    ADD CONSTRAINT test_process_test_fk FOREIGN KEY (test_id) REFERENCES test (id);

ALTER TABLE test_process
    ADD CONSTRAINT test_process_test_schedule_fk FOREIGN KEY (test_schedule_id) REFERENCES test_schedule (id);

