CREATE TABLE entity_group
(
    id  INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    criteria_matrix_id INTEGER NOT NULL,
    name VARCHAR(500),
    entity_ids json,

    CONSTRAINT entity_group_ibfk_1 FOREIGN KEY (criteria_matrix_id) REFERENCES criteria_matrix (id)
);
