CREATE TABLE test
(
    id            INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    opros_user_id INTEGER             NOT NULL,
    create_date   DATETIME            NOT NULL,
    update_date   DATETIME            NOT NULL ON UPDATE NOW(),

    name          TEXT,
    description   TEXT,

    deleted       TINYINT default 0,

    CONSTRAINT test_user_fk FOREIGN KEY (opros_user_id) REFERENCES opros_user (id)
);

CREATE TABLE test_question
(
    id            INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    test_id       INTEGER             NOT NULL,
    create_date   DATETIME            NOT NULL,
    update_date   DATETIME            NOT NULL ON UPDATE NOW(),

    question_text VARCHAR(1000),
    image_id      VARCHAR(100),

    mult_answers  TINYINT default 0,

    CONSTRAINT test_question_test_fk FOREIGN KEY (test_id) REFERENCES test (id)
);

CREATE TABLE question_answer
(
    id               INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    test_question_id INTEGER             NOT NULL,
    create_date      DATETIME            NOT NULL,
    update_date      DATETIME            NOT NULL ON UPDATE NOW(),

    answer_text      VARCHAR(1000),
    is_right         TINYINT default 0,

    CONSTRAINT answers_test_question_fk FOREIGN KEY (test_question_id) REFERENCES test_question (id)

)
