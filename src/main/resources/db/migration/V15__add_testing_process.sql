CREATE TABLE test_process
(
    id            INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    test_id       INTEGER             NOT NULL,
    user_id       INTEGER             NOT NULL,
    join_code     VARCHAR(255),
    create_date   DATETIME            NOT NULL,
    complete_date DATETIME,
    completed     TINYINT DEFAULT 0
);

CREATE TABLE tp_question
(
    id              INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    test_process_id INTEGER             NOT NULL,
    question_id     INTEGER             NOT NULL,
    completed       TINYINT DEFAULT 0,

    CONSTRAINT tp_question_test_process_fk FOREIGN KEY (test_process_id) REFERENCES test_process (id)
);

CREATE TABLE tp_answer
(
    id             INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    tp_question_id INTEGER             NOT NULL,
    answer_id      INTEGER             NOT NULL,

    CONSTRAINT tp_answer_tp_question_fk FOREIGN KEY (tp_question_id) REFERENCES tp_question (id)
);
