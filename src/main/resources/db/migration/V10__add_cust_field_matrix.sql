ALTER TABLE criteria_matrix
    ADD COLUMN max_answers_count INT default 0,
    ADD COLUMN custom INT default 0
