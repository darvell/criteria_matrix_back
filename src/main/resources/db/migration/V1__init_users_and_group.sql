CREATE TABLE opros_user
(
    id                      INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_name               VARCHAR(250)        NOT NULL,
    password                VARCHAR(250)        NOT NULL,
    enabled                 BOOLEAN             NOT NULL,
    account_non_expired     BOOLEAN             NOT NULL,
    account_non_locked      BOOLEAN             NOT NULL,
    credentials_non_expired BOOLEAN             NOT NULL
);


CREATE TABLE opros_role
(
    id   INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    role VARCHAR(250)        NOT NULL
);

CREATE TABLE opros_user_role
(
    opros_user_id INTEGER NOT NULL,
    opros_role_id INTEGER NOT NULL,
    PRIMARY KEY (opros_user_id, opros_role_id),
    FOREIGN KEY (opros_user_id) REFERENCES opros_user (id),
    FOREIGN KEY (opros_role_id) REFERENCES opros_role (id)
);

INSERT INTO opros_role (role)
    VALUE ('ROLE_USER');

INSERT INTO opros_role (role)
    VALUE ('ROLE_ADMIN');

INSERT INTO opros_user(id, user_name, password, enabled, account_non_expired, account_non_locked, credentials_non_expired)
VALUE(1,'andrew@darvell.ru', '$2a$10$SQk1EWJ9HV/qgOQAm4J/f.vNk.0zPgImM4Z.LCgFSP5z3YIA2s5c.', 1, 1, 1, 1);

INSERT INTO opros_user_role(opros_user_id, opros_role_id) VALUE (1,1);