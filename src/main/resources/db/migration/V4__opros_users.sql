CREATE TABLE opros
(
    id                 INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    create_date        DATE,
    criteria_matrix_id INTEGER             NOT NULL,
    open               TINYINT,

    CONSTRAINT opros_ibfk_1 FOREIGN KEY (criteria_matrix_id) REFERENCES criteria_matrix (id)
);

CREATE TABLE interviewee
(
    id                 INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    f VARCHAR(100),
    i VARCHAR(100),
    o VARCHAR(100),
    opros_id INTEGER             NOT NULL,
    answers json,
    CONSTRAINT interviewee_ibfk_1 FOREIGN KEY (opros_id) REFERENCES opros (id)
);