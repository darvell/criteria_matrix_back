CREATE TABLE criteria_matrix
(
    id  INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    opros_user_id INTEGER NOT NULL,
    max_member_count INTEGER,
    use_coefficient TINYINT,
    qr_link VARCHAR(500),
    name VARCHAR(500),

    CONSTRAINT criteria_matrix_fk FOREIGN KEY (opros_user_id) REFERENCES opros_user (id)
) ;

CREATE TABLE criteria
(
    id  INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    criteria_matrix_id INTEGER NOT NULL,
    value VARCHAR(500),
    CONSTRAINT criteria_ibfk_1 FOREIGN KEY (criteria_matrix_id) REFERENCES criteria_matrix (id)
) ;

CREATE TABLE matrix_entity
(
    id  INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    criteria_matrix_id INTEGER NOT NULL,
    value VARCHAR(500),

    CONSTRAINT matrix_entity_ibfk_1 FOREIGN KEY (criteria_matrix_id) REFERENCES criteria_matrix (id)
);