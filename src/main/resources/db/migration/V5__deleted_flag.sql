ALTER TABLE criteria_matrix
    ADD COLUMN deleted TINYINT default 0;

ALTER TABLE opros
    ADD COLUMN deleted TINYINT default 0;