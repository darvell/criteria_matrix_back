package ru.darvell.rostelecom.oprosservice.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.model.MatrixEntity;

import java.util.Optional;

@Repository
public interface MatrixEntityRepository extends JpaRepository<MatrixEntity, Long> {
    Optional<MatrixEntity> findByIdAndCriteriaMatrix(long id, CriteriaMatrix criteriaMatrix);
}
