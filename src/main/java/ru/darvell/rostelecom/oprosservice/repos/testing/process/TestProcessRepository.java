package ru.darvell.rostelecom.oprosservice.repos.testing.process;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TestProcess;

import java.util.List;

@Repository
public interface TestProcessRepository extends JpaRepository<TestProcess, Long> {

    List<TestProcess> findAllByTestAndUserId(Test test, Long userId);

    List<TestProcess> findAllByUserId(Long userId);
}
