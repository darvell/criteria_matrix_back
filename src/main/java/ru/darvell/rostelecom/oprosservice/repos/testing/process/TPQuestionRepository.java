package ru.darvell.rostelecom.oprosservice.repos.testing.process;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TPQuestion;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TestProcess;

import java.util.List;
import java.util.Optional;

@Repository
public interface TPQuestionRepository extends JpaRepository<TPQuestion, Long> {

    List<TPQuestion> findAllByTestProcess(TestProcess testProcess);
    Optional<TPQuestion> findByTestProcessAndId(TestProcess testProcess, long id);
}
