package ru.darvell.rostelecom.oprosservice.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface CriteriaMatrixRepository extends JpaRepository<CriteriaMatrix, Long> {

    List<CriteriaMatrix> getAllByUser(User user);
    Optional<CriteriaMatrix> findByIdAndUser(long id, User user);
}
