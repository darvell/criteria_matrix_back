package ru.darvell.rostelecom.oprosservice.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.Opros;

@Repository
public interface OprosRepository extends JpaRepository<Opros, Long> {
}
