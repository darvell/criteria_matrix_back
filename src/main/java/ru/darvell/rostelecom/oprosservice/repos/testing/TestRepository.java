package ru.darvell.rostelecom.oprosservice.repos.testing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;

import java.util.List;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {

    List<Test> getAllByTestOwnerUser(User user);
}
