package ru.darvell.rostelecom.oprosservice.repos.testing;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;

import java.util.List;
import java.util.Optional;

public interface TestQuestionRepository extends JpaRepository<TestQuestion, Long> {

    List<TestQuestion> findAllByTest(Test test);
}
