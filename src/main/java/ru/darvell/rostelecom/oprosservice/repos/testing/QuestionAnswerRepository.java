package ru.darvell.rostelecom.oprosservice.repos.testing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.testing.QuestionAnswer;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;

import java.util.List;

@Repository
public interface QuestionAnswerRepository extends JpaRepository<QuestionAnswer, Long> {
    List<QuestionAnswer> getAllByQuestion(TestQuestion question);
}
