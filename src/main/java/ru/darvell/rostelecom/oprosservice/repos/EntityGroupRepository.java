package ru.darvell.rostelecom.oprosservice.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.darvell.rostelecom.oprosservice.model.EntityGroup;

public interface EntityGroupRepository extends JpaRepository<EntityGroup, Long> {
}
