package ru.darvell.rostelecom.oprosservice.repos.testing.process;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TPAnswer;

@Repository
public interface TestProcessAnswerRepository extends JpaRepository<TPAnswer, Long> {
}
