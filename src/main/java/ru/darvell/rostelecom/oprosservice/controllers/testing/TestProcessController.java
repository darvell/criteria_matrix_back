package ru.darvell.rostelecom.oprosservice.controllers.testing;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TestProcess;
import ru.darvell.rostelecom.oprosservice.services.testing.process.UserTestService;
import ru.darvell.rostelecom.oprosservice.web.models.testing.user.TestInAction;
import ru.darvell.rostelecom.oprosservice.web.models.testing.user.TestProcessResponse;
import ru.darvell.rostelecom.oprosservice.web.models.testing.user.TestResponse;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping(value = "/do_test")
public class TestProcessController {

    private final UserTestService testService;

    @GetMapping(value = "/{testId}/active")
    public TestInAction getActiveTestProcess(@PathVariable(name = "testId") long testId) {

        TestInAction testInAction = testService.fillTestInAction(testId);

        if (testInAction == null) {
            throw new NotFoundException("not found");
        }
        List<TestProcessResponse> testProcess = testService.getActiveProcessByTestId(testId)
                .stream()
                .filter(tp -> !tp.isDeleted())
                .map(TestProcessResponse::new)
                .collect(Collectors.toList());

        testInAction.setTestProcessList(testProcess);
        return testInAction;
    }

    @GetMapping(value = "/active")
    public List<TestInAction> getAllActiveTestProcess() {
        List<TestInAction> response = new LinkedList<>();

        List<TestProcess> testProcess = testService.getActiveProcessByCurrUser()
                .stream()
                .filter(tp -> !tp.isDeleted())
                .collect(Collectors.toList());

        if (testProcess.size() > 0) {
            for (Test test : testProcess.stream()
                    .map(TestProcess::getTest)
                    .collect(Collectors.toSet())) {

                TestInAction testInAction = new TestInAction(test);
                testInAction.setTestProcessList(testProcess.stream()
                        .filter(tp -> tp.getTest().equals(test))
                        .map(TestProcessResponse::new)
                        .collect(Collectors.toList())
                );
                response.add(testInAction);
            }
        }
        return response;
    }

    @GetMapping(value = "/{testId}/join")
    public TestProcess joinToTest(@PathVariable(name = "testId") long testId) {
        return testService.joinToTest(testId);
    }

    @GetMapping(value = "/{processId}")
    public TestResponse getTest(@PathVariable(name = "processId") long testProcessId) {
        return testService.collectDataForTest(testProcessId);
    }

    @PostMapping(value = "/{processId}/{questionId}")
    public ResponseEntity<HttpStatus> addAnswer(@PathVariable(name = "processId") long processId,
                                                @PathVariable(name = "questionId") long questionId,
                                                @RequestBody List<Long> answers) {

        testService.addAnswer(processId, questionId, answers);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/{processId}/finish")
    public ResponseEntity<HttpStatus> finishTest(@PathVariable(name = "processId") long processId){
        testService.finishTest(processId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
