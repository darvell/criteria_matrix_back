package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.model.Criteria;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.model.EntityGroup;
import ru.darvell.rostelecom.oprosservice.model.MatrixEntity;
import ru.darvell.rostelecom.oprosservice.services.MatrixAdminService;
import ru.darvell.rostelecom.oprosservice.web.models.EntityGroupAddRequest;
import ru.darvell.rostelecom.oprosservice.web.models.MatrixAddRequest;
import ru.darvell.rostelecom.oprosservice.web.models.MatrixResponse;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/matrixAdmin")
@CrossOrigin
@AllArgsConstructor
public class CriteriaMatrixAdminController {

    private MatrixAdminService matrixAdminService;

//    @PostMapping(value = "")
//    ResponseEntity<MatrixResponse> createNewCriteriaMatrix(@RequestParam(name = "name") String name,
//                                                           @RequestParam(name = "useCoefficient") Boolean useCoefficient) {
//        return new ResponseEntity<>(new MatrixResponse(matrixAdminService.createNewMatrix(name, useCoefficient)), HttpStatus.OK);
//    }

    @PostMapping(value = "")
    HttpStatus createNewCriteriaMatrix(@RequestBody MatrixAddRequest matrixAddRequest) {
        matrixAdminService.createNewMatrix(matrixAddRequest);
        return HttpStatus.OK;
    }

    @DeleteMapping(value = "/{matrixId}")
    HttpStatus deleteMatrix(@PathVariable(name = "matrixId") long matrixId){
        if (matrixAdminService.deleteMatrix(matrixId)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.BAD_REQUEST;
        }
    }

    @PutMapping(value = "/{matrixId}")
    ResponseEntity<MatrixResponse> updateCriteriaMatrix(@PathVariable(name = "matrixId") long id,
                                                        @RequestParam(name = "name", required = false) String name,
                                                        @RequestParam(name = "useCoefficient", required = false) Boolean useCoefficient) {
        return new ResponseEntity<>(new MatrixResponse(matrixAdminService.updateMatrix(id, name, useCoefficient)), HttpStatus.OK);
    }

    @PostMapping(value = "/{matrixId}/criteria")
    ResponseEntity<MatrixResponse> addCriteriasToMatrix(@PathVariable(name = "matrixId") long id,
                                                        @RequestBody List<Criteria> criteriaList) {
        return new ResponseEntity<>(new MatrixResponse(matrixAdminService.addCriterias(id, criteriaList)), HttpStatus.OK);
    }

    @PostMapping(value = "/{matrixId}/add/criteria")
    ResponseEntity<Criteria> addOneCriteriaToMatrix(@PathVariable(name = "matrixId") long id,
                                                        @RequestParam(name = "value") String value,
                                                          @RequestParam(name ="weight", required = false) Double weight) {
        return new ResponseEntity<>(matrixAdminService.addCriteria(id, value, weight), HttpStatus.OK);
    }

    @PostMapping(value = "/{matrixId}/add/entity")
    ResponseEntity<MatrixEntity> addOneEntityToMatrix(@PathVariable(name = "matrixId") long id,
                                                    @RequestParam(name = "value") String value){
        return new ResponseEntity<>(matrixAdminService.addEntity(id, value), HttpStatus.OK);
    }

    @PostMapping(value = "/{matrixId}/entity")
    ResponseEntity<MatrixResponse> addEntitysToMatrix(@PathVariable(name = "matrixId") long id,
                                                      @RequestBody List<MatrixEntity> entityList) {
        return new ResponseEntity<>(new MatrixResponse(matrixAdminService.addEntitys(id, entityList)), HttpStatus.OK);
    }

    @PostMapping(value = "/{matrixId}/entity/group")
    ResponseEntity<EntityGroup> addEntityGroupToMatrix(@PathVariable(name = "matrixId") long id,
                                                       @RequestBody EntityGroupAddRequest request) {
        return new ResponseEntity<>(matrixAdminService.addEntityGroup(id, request), HttpStatus.OK);
    }

    @PutMapping(value = "/criteria/{criteriaId}")
    HttpStatus updateCriteria(@PathVariable(name = "criteriaId") long criteriaId,
                              @RequestParam(name = "value") String value,
                              @RequestParam(name = "weight", required = false) Double weight) {
        if (matrixAdminService.updateCriteriaValue(criteriaId, value, weight)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.BAD_REQUEST;
        }
    }

    @PutMapping(value = "/entity/{entityId}")
    HttpStatus updateMatrixEntity(@PathVariable(name = "entityId") long entityId,
                                  @RequestParam(name = "value") String value) {
        if (matrixAdminService.updateMatrixEntityValue(entityId, value)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.BAD_REQUEST;
        }
    }


    @GetMapping(value = "/{matrixId}")
    ResponseEntity<MatrixResponse> findCriteriaMatrixById(@PathVariable(name = "matrixId") long id) {
        CriteriaMatrix matrix = matrixAdminService.getByIdSecured(id);
        return new ResponseEntity<>(new MatrixResponse(matrix), HttpStatus.OK);
    }

    @GetMapping(value = "")
    ResponseEntity<List<MatrixResponse>> findAll() {
        return new ResponseEntity<>(matrixAdminService.getAllByUser().stream()
                .map(MatrixResponse::new)
                .collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @GetMapping(value = "/{matrixId}/new_opros")
    ResponseEntity<MatrixResponse> newOpros(@PathVariable(name = "matrixId") long matrixId) {
        return new ResponseEntity<>(new MatrixResponse(matrixAdminService.newOpros(matrixId)), HttpStatus.OK);
    }


}
