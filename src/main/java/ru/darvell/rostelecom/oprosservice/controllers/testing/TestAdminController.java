package ru.darvell.rostelecom.oprosservice.controllers.testing;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.model.testing.QuestionAnswer;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;
import ru.darvell.rostelecom.oprosservice.services.testing.TestingAdminService;
import ru.darvell.rostelecom.oprosservice.web.models.testing.UpdateTestRequest;
import ru.darvell.rostelecom.oprosservice.web.models.testing.admin.QuestionRA;
import ru.darvell.rostelecom.oprosservice.web.models.testing.admin.TestListResponse;
import ru.darvell.rostelecom.oprosservice.web.models.testing.admin.TestResponse;

import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping(value = "/testing")
public class TestAdminController {

    private final TestingAdminService testingAdminService;

    @GetMapping("/all")
    public TestListResponse getAllTests() {
        return new TestListResponse(testingAdminService.getAll());
    }

    @GetMapping("/{testId}")
    public TestResponse getTestById(@PathVariable(name = "testId") long testId) {
        return new TestResponse(testingAdminService.getById(testId));

    }

    @PostMapping("/{testId}")
    public TestResponse saveTestInfo(@PathVariable(name = "testId") long testId,
                                     @RequestBody UpdateTestRequest testRequest) {
        return new TestResponse(testingAdminService.updateTest(testId, testRequest));

    }

    @PostMapping("/")
    public TestResponse addNewTest(@RequestBody UpdateTestRequest testRequest) {
        return new TestResponse(testingAdminService.createNewTest(testRequest));
    }

    @GetMapping("/{testId}/questions")
    public List<TestQuestion> getQuestionsByTest(@PathVariable(name = "testId") long testId) {
        return testingAdminService.getQuestionsByTest(testId);
    }

    @GetMapping("/questions/{questionId}")
    public QuestionRA getQuestion(@PathVariable(name = "questionId") long questionId) {
        QuestionRA response = new QuestionRA();
        TestQuestion question = testingAdminService.getQuestionById(questionId);
        List<QuestionAnswer> answers = testingAdminService.getAnswers(question);
        response.setQuestion(question);
        response.setAnswers(answers);
        return response;
    }

    @PostMapping("/questions/{questionId}")
    public QuestionRA updateQuestion(@PathVariable(name = "questionId") long questionId,
                                     @RequestBody QuestionRA request) {
        TestQuestion question = testingAdminService.updateQuestion(questionId, request);
        QuestionRA response = new QuestionRA();
        response.setQuestion(question);
        response.setAnswers(testingAdminService.getAnswers(question));
        return response;
    }

    @PostMapping("/{testId}/questions")
    public QuestionRA addQuestion(@PathVariable(name = "testId") long testId,
                                      @RequestBody QuestionRA request) {
        TestQuestion question = testingAdminService.addQuestion(testId, request);
        QuestionRA response = new QuestionRA();
        response.setQuestion(question);
        response.setAnswers(testingAdminService.getAnswers(question));
        return response;
    }

    @DeleteMapping("/answer/{answerId}")
    public ResponseEntity deleteAnswer(@PathVariable(name = "answerId") long answerId) {
        testingAdminService.deleteAnswer(answerId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{testId}")
    public ResponseEntity deleteTest(@PathVariable(name = "testId") long testId) {
        testingAdminService.deleteTest(testId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
