package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.darvell.rostelecom.oprosservice.services.MatrixAdminService;

import java.awt.image.BufferedImage;


@RestController
@AllArgsConstructor
@RequestMapping("/qrcode")
public class QRCodeController {

    private MatrixAdminService matrixAdminService;

    @GetMapping(value = "/opros/{oprosId}", produces = MediaType.IMAGE_PNG_VALUE)
    ResponseEntity<BufferedImage> getJoinQRcode(@PathVariable(name = "oprosId") long oprosId) {
        return new ResponseEntity<>(matrixAdminService.genQrCode(oprosId), HttpStatus.OK);
    }

    @Bean
    public HttpMessageConverter<BufferedImage> createImageHttpMessageConverter() {
        return new BufferedImageHttpMessageConverter();
    }
}
