package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.services.TokenAuthService;
import ru.darvell.rostelecom.oprosservice.services.UsersService;
import ru.darvell.rostelecom.oprosservice.web.models.LoginResponse;

@RestController
@RequestMapping(value = "/register")
@AllArgsConstructor
@CrossOrigin
public class RegisterController {

    private final TokenAuthService tokenAuthService;
    private final UsersService usersService;

    @PostMapping
    public ResponseEntity<LoginResponse> authUser(@RequestParam(value = "login") String login,
                                                  @RequestParam(value = "password") String password) {
        usersService.addUser(login, password);
        return new ResponseEntity<>(new LoginResponse(tokenAuthService.authUser(login, password)),
                HttpStatus.OK);
    }
}
