package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.model.Interviewee;
import ru.darvell.rostelecom.oprosservice.services.IntervieweeProcessService;
import ru.darvell.rostelecom.oprosservice.web.models.Answer;
import ru.darvell.rostelecom.oprosservice.web.models.JoinOprosResponse;
import ru.darvell.rostelecom.oprosservice.web.models.MatrixQuestionResponse;

import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping(value = "/interview")
public class InterviewController {

    private final IntervieweeProcessService intervieweeProcessService;

    @GetMapping(value = "/opros/{oprosId}/join")
    ResponseEntity<JoinOprosResponse> joinToOpros(@PathVariable(name = "oprosId") long oprosId) {
        Interviewee interviewee = intervieweeProcessService.joinOpros(oprosId);
        JoinOprosResponse joinOprosResponse = new JoinOprosResponse();
        if (interviewee != null) {
            joinOprosResponse.setInterviewId(interviewee.getId());
            joinOprosResponse.setOprosId(interviewee.getOpros().getId());
            joinOprosResponse.setSuccess(true);
        } else {
            joinOprosResponse.setSuccess(false);
        }
        return new ResponseEntity<>(joinOprosResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/opros/{oprosId}/is_open")
    ResponseEntity<Void> isOprosOpen(@PathVariable(name = "oprosId") long oprosId) {
        if (intervieweeProcessService.isOprosOpen(oprosId)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/{interviewId}/fio")
    ResponseEntity<MatrixQuestionResponse> setFio(@PathVariable(value = "interviewId") long interviewId,
                                                  @RequestParam(name = "f") String f,
                                                  @RequestParam(name = "i") String i,
                                                  @RequestParam(name = "o") String o) {
        intervieweeProcessService.setFIO(interviewId, f, i, o);

        return new ResponseEntity<>(intervieweeProcessService.getMatrixQuestionsByInterview(interviewId), HttpStatus.OK);

    }

    @GetMapping(value = "/{interviewId}/continue")
    ResponseEntity<MatrixQuestionResponse> continueInterview(@PathVariable(value = "interviewId") long interviewId
    ) {
        return new ResponseEntity<>(intervieweeProcessService.getMatrixQuestionsByInterview(interviewId), HttpStatus.OK);

    }

    @GetMapping(value = "/{interviewId}/can_continue")
    ResponseEntity<Void> canContinueInterview(@PathVariable(value = "interviewId") long interviewId
    ) {
        if (intervieweeProcessService.canContinue(interviewId)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/getQuestions/{oprosId}")
    ResponseEntity<MatrixQuestionResponse> getMatrixQuestions(@PathVariable(value = "oprosId") long oprosId) {
        return new ResponseEntity<>(intervieweeProcessService.getMatrixQuestionsByOpros(oprosId), HttpStatus.OK);
    }

    @PostMapping(value = "{interviewId}/criteria/{criteriaId}/answers")
    HttpStatus addAnswers(@PathVariable(name = "interviewId") long interviewId,
                          @PathVariable(name = "criteriaId") long criteriaId,
                          @RequestBody List<Answer> answers) {
        intervieweeProcessService.publicAnswer(interviewId, criteriaId, answers);
        return HttpStatus.OK;
    }


}
