package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.services.MatrixAdminService;
import ru.darvell.rostelecom.oprosservice.web.models.MatrixResponse;
import ru.darvell.rostelecom.oprosservice.web.models.martixresult.ResultMatrixResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
@CrossOrigin
@RequestMapping(value = "/opros")
@AllArgsConstructor
public class OprosAdminController {

    private final MatrixAdminService matrixAdminService;

    @GetMapping(value = "/{oprosId}/stop")
    public ResponseEntity<MatrixResponse> stopOpros(@PathVariable(name = "oprosId") long oprosId) {
        return new ResponseEntity<>(new MatrixResponse(matrixAdminService.stopOpros(oprosId)), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{oprosId}")
    public ResponseEntity<MatrixResponse> deleteOpros(@PathVariable(name = "oprosId") long oprosId) {
        return new ResponseEntity<>(new MatrixResponse(matrixAdminService.deleteOpros(oprosId)), HttpStatus.OK);
    }

    @GetMapping(value = "/{oprosId}/get_result")
    ResponseEntity<ResultMatrixResponse> getResultByOpros(@PathVariable(name = "oprosId") long oprosId) {
        return new ResponseEntity<>(matrixAdminService.calculateMatrixByOprosId(oprosId), HttpStatus.OK);
    }

    @GetMapping(value = "/{oprosId}/get_result/xls")
    public void downloadXls(@PathVariable(name = "oprosId") long oprosId,
                         HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=table.xlsx");
        ByteArrayInputStream stream = matrixAdminService.generateExcell(oprosId);
        IOUtils.copy(stream, response.getOutputStream());

    }

    @GetMapping(value = "/{oprosId}/interviewCount")
    public ResponseEntity<Long> getInterviewCount(@PathVariable(name = "oprosId") long oprosId) {
        return new ResponseEntity<>(matrixAdminService.calculateInterviewCount(oprosId), HttpStatus.OK);
    }
}
