package ru.darvell.rostelecom.oprosservice.controllers;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
public class TestController {

    @GetMapping("/test")
    @ResponseBody
    public String test() {
        return "status=1";
    }
}
