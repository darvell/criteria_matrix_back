package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.UserRole;
import ru.darvell.rostelecom.oprosservice.services.UsersService;

import java.util.List;

@RestController
@RequestMapping(value = "/admin")
@CrossOrigin
@AllArgsConstructor
public class AdminController {

    private UsersService usersService;

    @GetMapping(value = "/users")
    ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(usersService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping(value = "/users/roles")
    ResponseEntity<List<UserRole>> getAllUserRoles() {
        return new ResponseEntity<>(usersService.getPossibleRoles(), HttpStatus.OK);
    }

    @PostMapping(value = "/users")
    ResponseEntity<User> addUser(@RequestBody User user) {
        return new ResponseEntity<>(usersService.addUser(user), HttpStatus.OK);
    }

    @PutMapping(value = "/users/{userId}")
    ResponseEntity<User> updateUser(@PathVariable(name = "userId") long userId,
                                    @RequestBody User user) {
        return new ResponseEntity<>(usersService.updateUser(userId, user), HttpStatus.OK);
    }
}
