package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.services.TokenAuthService;
import ru.darvell.rostelecom.oprosservice.services.UsersService;
import ru.darvell.rostelecom.oprosservice.web.models.TokenCheckResponse;

@RestController
@RequestMapping(value = "/token")
@AllArgsConstructor
@CrossOrigin
public class TokenController {

    private TokenAuthService tokenAuthService;
    private UsersService usersService;

    @PostMapping(value = "/check")
    ResponseEntity<TokenCheckResponse> checkToken(@RequestHeader(name = "X-Auth-token") String token) {
        User user = usersService.findById(tokenAuthService.getUserIdByToken(token));
        return new ResponseEntity<>(new TokenCheckResponse(
                user,
                tokenAuthService.updateToken(user)
        ), HttpStatus.OK);
    }
}
