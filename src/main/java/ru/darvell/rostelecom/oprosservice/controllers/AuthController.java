package ru.darvell.rostelecom.oprosservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.darvell.rostelecom.oprosservice.services.TokenAuthService;
import ru.darvell.rostelecom.oprosservice.web.models.LoginResponse;

@RestController
@RequestMapping(value = "/auth")
@AllArgsConstructor
@CrossOrigin
public class AuthController {

    private final TokenAuthService tokenAuthService;

    @PostMapping
    public ResponseEntity<LoginResponse> authUser(@RequestParam(value = "login") String login,
                                                  @RequestParam(value = "password") String password) {
        return new ResponseEntity<>(new LoginResponse(tokenAuthService.authUser(login, password)),
                HttpStatus.OK);
    }
}
