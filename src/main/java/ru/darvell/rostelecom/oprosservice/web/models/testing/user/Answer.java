package ru.darvell.rostelecom.oprosservice.web.models.testing.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Answer {

    private long id;
    private String text;
    private boolean checked;
}
