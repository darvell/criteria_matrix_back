package ru.darvell.rostelecom.oprosservice.web.models.testing.user;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
public class Question {

    private long id;
    private String text;

    private List<Answer> answers = new LinkedList<>();

    public void addAnswer(Answer answer) {
        answers.add(answer);
    }

}
