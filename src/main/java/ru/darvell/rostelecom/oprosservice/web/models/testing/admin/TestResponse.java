package ru.darvell.rostelecom.oprosservice.web.models.testing.admin;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@ToString
public class TestResponse {

    private long id;
    private String name;
    private String description;
    private Timestamp createDate;

    public TestResponse(Test test){
        this.id = test.getId();
        this.name = test.getName();
        this.description = test.getDescription();
        this.createDate = test.getCreateDate();
    }
}
