package ru.darvell.rostelecom.oprosservice.web.models.martixresult;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class ResultMatrixResponse {
    private Map<Long, Map<Long, Double>> resultMatrix;
    private Map<Long, String> criteriaLegend;
    private Map<Long, String> entityLegend;
    private Map<Long, Double> weights;
}
