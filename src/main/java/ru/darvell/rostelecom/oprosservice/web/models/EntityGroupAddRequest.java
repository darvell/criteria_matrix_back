package ru.darvell.rostelecom.oprosservice.web.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EntityGroupAddRequest {
    private String name;
    private List<Long> entityIds;
}
