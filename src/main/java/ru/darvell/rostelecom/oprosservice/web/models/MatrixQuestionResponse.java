package ru.darvell.rostelecom.oprosservice.web.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.darvell.rostelecom.oprosservice.model.Criteria;
import ru.darvell.rostelecom.oprosservice.model.EntityGroup;
import ru.darvell.rostelecom.oprosservice.model.MatrixEntity;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class MatrixQuestionResponse {

    private long interviewId;

    private List<Criteria> criterias;
    private List<MatrixEntity> entitys;
    private List<EntityGroup> entityGroups;

    private boolean needMax;
    private boolean needMin;
    private boolean partAnswer;
    private int maxRaiting;
    private String actionText;
    private int maxAnswersCount;
    private int custom;
}
