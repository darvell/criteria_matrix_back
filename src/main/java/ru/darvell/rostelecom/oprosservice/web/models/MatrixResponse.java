package ru.darvell.rostelecom.oprosservice.web.models;

import lombok.Getter;
import lombok.Setter;
import ru.darvell.rostelecom.oprosservice.model.*;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class MatrixResponse {
    private Long id;
    private int maxMembersCount;
    private boolean useCoefficient;
    private String name;
    private List<Criteria> criteriaSet;
    private List<MatrixEntity> matrixEntities;
    private List<Opros> oprosList;
    private boolean deleted;
    private boolean needMax;
    private boolean needMin;
    private boolean partAnswer;
    private int maxRating;
    private String actionText;
    private int maxAnswersCount;
    private int custom;
    private List<EntityGroup> entityGroups;

    public MatrixResponse(CriteriaMatrix eden) {
        this.id = eden.getId();
        this.maxMembersCount = eden.getMaxMembersCount();
        this.useCoefficient = eden.isUseCoefficient();
        this.name = eden.getName();
        this.setCriteriaSet(eden.getCriteriaSet());
        this.setMatrixEntities(eden.getMatrixEntities());
        if (eden.getOprosList() != null) {
            this.oprosList = eden.getOprosList().stream().filter(o -> !o.isDeleted()).collect(Collectors.toList());
            this.getOprosList().forEach(o -> o.setIntervieweesCount((int) o.getInterviewees()
                    .stream()
                    .filter(i -> i.getAnswers() != null).count()));
        }
        this.deleted = eden.isDeleted();
        this.needMax = eden.isNeedMax();
        this.needMin = eden.isNeedMin();
        this.partAnswer = eden.isPartAnswer();
        this.maxRating = eden.getMaxRating();
        this.actionText = eden.getActionText();
        this.maxAnswersCount = eden.getMaxAnswersCount();
        this.custom = eden.getCustom();
        this.setEntityGroups(eden.getEntityGroups());
    }
}
