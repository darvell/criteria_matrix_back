package ru.darvell.rostelecom.oprosservice.web.models.testing.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestInAction {

    private long testId;
    private String testName;
    private boolean complete;
    private List<TestProcessResponse> testProcessList;

    public TestInAction(Test test) {
        this.testId = test.getId();
        this.testName = test.getName();
    }

    public void setTestProcessList(List<TestProcessResponse> testProcessList) {
        this.testProcessList = testProcessList;
        complete = testProcessList.stream().anyMatch(TestProcessResponse::isComplete);
    }
}
