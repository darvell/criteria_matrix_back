package ru.darvell.rostelecom.oprosservice.web.models.testing.user;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
public class TestResponse {
    private long id;
    private List<Question> questions = new LinkedList<>();

    public void addQuestion(Question question) {
        questions.add(question);
    }
}
