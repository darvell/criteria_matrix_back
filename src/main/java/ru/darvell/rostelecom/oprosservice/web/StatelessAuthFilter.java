package ru.darvell.rostelecom.oprosservice.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import ru.darvell.rostelecom.oprosservice.services.TokenAuthService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

public class StatelessAuthFilter extends GenericFilterBean {

    @Autowired
    private final TokenAuthService tokenAuthService;

    public StatelessAuthFilter(TokenAuthService tokenAuthService) {
        this.tokenAuthService = tokenAuthService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Enumeration<String> parNames  = request.getParameterNames();
//        while (parNames.asIterator().hasNext()){
//            System.out.println(parNames.asIterator().next());
//        }
//
//        System.out.println(((HttpServletRequest)request).getRequestURI());
        SecurityContextHolder.getContext().setAuthentication(
                tokenAuthService.getAuthentication((HttpServletRequest) request).orElse(null));
        chain.doFilter(request, response);
    }
}

