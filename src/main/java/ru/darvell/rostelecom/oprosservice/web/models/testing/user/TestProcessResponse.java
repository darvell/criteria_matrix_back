package ru.darvell.rostelecom.oprosservice.web.models.testing.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TestProcess;

import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestProcessResponse {

    private long id;
    private Timestamp startDate;

    private boolean complete;

    public TestProcessResponse(TestProcess test) {
        id = test.getId();
        startDate = test.getCreateDate();
        complete = test.isCompleted();
    }
}
