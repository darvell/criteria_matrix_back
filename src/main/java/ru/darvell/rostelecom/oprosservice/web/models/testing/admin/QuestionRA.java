package ru.darvell.rostelecom.oprosservice.web.models.testing.admin;

import lombok.*;
import ru.darvell.rostelecom.oprosservice.model.testing.QuestionAnswer;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuestionRA {

    private TestQuestion question;
    private List<QuestionAnswer> answers;

    public void setAnswers(List<QuestionAnswer> answers) {
        this.answers = answers.stream().filter(a -> !a.isDeleted()).collect(Collectors.toList());
    }

}
