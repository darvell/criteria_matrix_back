package ru.darvell.rostelecom.oprosservice.web.models.testing.admin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TestListResponse {

    private List<Test> tests;

    public TestListResponse(List<Test> tests) {
        this.tests = tests.stream()
                .filter(t -> !t.isDeleted())
                .collect(Collectors.toList());
    }

}
