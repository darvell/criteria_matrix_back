package ru.darvell.rostelecom.oprosservice.web.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class JoinOprosResponse {
    private boolean success;
    private long interviewId;
    private long oprosId;
}
