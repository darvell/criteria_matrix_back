package ru.darvell.rostelecom.oprosservice.web.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.UserRole;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TokenCheckResponse {
    private Long userId;
    private String[] roles;
    private String token;

    public TokenCheckResponse(User user, String token){
        userId = user.getId();
        this.token = token;
        roles = user.getAuthorities().stream()
                .map(UserRole::getRole)
                .toArray(String[]::new);
    }
}
