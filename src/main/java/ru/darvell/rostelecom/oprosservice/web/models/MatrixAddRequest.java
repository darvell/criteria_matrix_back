package ru.darvell.rostelecom.oprosservice.web.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.darvell.rostelecom.oprosservice.model.Criteria;
import ru.darvell.rostelecom.oprosservice.model.MatrixEntity;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MatrixAddRequest {
    private String name;
    private boolean useCoefficient;
    private List<MatrixEntity> entityList;
    private List<Criteria> criteriaList;
    private boolean needMax;
    private boolean needMin;
    private boolean partAnswer;
    private int maxRating;
}
