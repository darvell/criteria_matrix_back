package ru.darvell.rostelecom.oprosservice.web.models.testing;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateTestRequest {

    private String name;
    private String description;

}
