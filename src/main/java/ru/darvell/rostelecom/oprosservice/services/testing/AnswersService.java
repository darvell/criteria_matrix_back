package ru.darvell.rostelecom.oprosservice.services.testing;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.testing.QuestionAnswer;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;
import ru.darvell.rostelecom.oprosservice.repos.testing.QuestionAnswerRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class AnswersService {

    private final QuestionAnswerRepository questionAnswerRepository;

    public List<QuestionAnswer> getAllByQuestion(TestQuestion question) {
        return questionAnswerRepository.getAllByQuestion(question);
    }

    QuestionAnswer saveAndFlush(QuestionAnswer answer) {
        return questionAnswerRepository.saveAndFlush(answer);
    }

    void deleteAnswer(QuestionAnswer answer) {
        answer.setDeleted(true);
        saveAndFlush(answer);
    }

    QuestionAnswer getById(long id) {
        return questionAnswerRepository.findById(id).orElseThrow(
                NotFoundException::new);
    }

}
