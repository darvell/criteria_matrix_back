package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.config.UserAuthentication;
import ru.darvell.rostelecom.oprosservice.exceptions.UserNotFoundException;
import ru.darvell.rostelecom.oprosservice.model.User;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TokenAuthService {

    private static final String AUTH_HEADER_NAME = "X-Auth-Token";

    private TokenHandler tokenHandler;

    private UsersService usersService;

    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        return Optional
                .ofNullable(request.getHeader(AUTH_HEADER_NAME))
                .flatMap(tokenHandler::extrackUserId)
                .map(usersService::findById)
                .map(UserAuthentication::new);
    }

    public String authUser(String userName, String password) throws BadCredentialsException {
        return tokenHandler.generateAccessToken(
                usersService.checkUserCredentials(userName, password)
                        .orElseThrow(UserNotFoundException::new)
                , LocalDateTime.now().plusDays(10));
    }

    public String updateToken(User user){
        return tokenHandler.generateAccessToken(user, LocalDateTime.now().plusDays(10));
    }

    public Long getUserIdByToken(String token){
        return tokenHandler.extrackUserId(token)
                .orElseThrow(UserNotFoundException::new);
    }
}
