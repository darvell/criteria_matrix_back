package ru.darvell.rostelecom.oprosservice.services.testing;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;
import ru.darvell.rostelecom.oprosservice.repos.testing.TestQuestionRepository;

import java.sql.Timestamp;
import java.util.List;

@Service
@AllArgsConstructor
public class TestQuestionService {
    private final TestQuestionRepository testQuestionRepository;

    public List<TestQuestion> findAllByTest(Test test) {
        return testQuestionRepository.findAllByTest(test);
    }

    public TestQuestion findById(long questionId) throws NotFoundException {
        return testQuestionRepository.findById(questionId).orElseThrow(() -> new NotFoundException("Question not found"));
    }

    TestQuestion saveAndFlush(TestQuestion question) {
        if (question.getCreateDate() == null) {
            question.setCreateDate(new Timestamp(System.currentTimeMillis()));
        }
        question.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        return testQuestionRepository.saveAndFlush(question);
    }
}
