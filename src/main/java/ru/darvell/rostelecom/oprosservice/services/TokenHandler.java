package ru.darvell.rostelecom.oprosservice.services;

import com.google.common.io.BaseEncoding;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;
import ru.darvell.rostelecom.oprosservice.model.User;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class TokenHandler {

    private final SecretKey secretKey;
    private final String ROLE_KEY = "roles";

    public TokenHandler() {
        String jwtKey = "lniwerg34ScafdsfpoPiw3";
        byte[] decodedKey = BaseEncoding.base64().decode(jwtKey);
        secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }

    Optional<Long> extrackUserId(String token) {
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims body = claimsJws.getBody();
            return Optional
                    .ofNullable(body.getId())
                    .map(Long::valueOf);
        } catch (RuntimeException e) {
            return Optional.empty();
        }
    }

    String generateAccessToken(User user, LocalDateTime expires) {
        return Jwts.builder()
                .setId(String.valueOf(user.getId()))
                .setExpiration(Date.from(expires.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }

    Map<String, Object> convertAuthoririesToClaim(User user){
        Map<String, Object> map = new HashMap<>();
        map.put(ROLE_KEY, user.getAuthorities().toArray());
        return map;
    }
}
