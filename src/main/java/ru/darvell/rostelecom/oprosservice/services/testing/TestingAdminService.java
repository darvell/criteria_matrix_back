package ru.darvell.rostelecom.oprosservice.services.testing;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.AccessDeniedException;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.testing.QuestionAnswer;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;
import ru.darvell.rostelecom.oprosservice.services.SecureService;
import ru.darvell.rostelecom.oprosservice.web.models.testing.UpdateTestRequest;
import ru.darvell.rostelecom.oprosservice.web.models.testing.admin.QuestionRA;
import ru.darvell.rostelecom.oprosservice.web.models.testing.admin.TestResponse;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TestingAdminService {

    private final TestService testService;
    private final SecureService secureService;
    private final TestQuestionService testQuestionService;
    private final AnswersService answersService;

    public Test createNewTest(UpdateTestRequest updateTestRequest) {
        Test test = Test.builder()
                .name(updateTestRequest.getName())
                .description(updateTestRequest.getDescription())
                .testOwnerUser(secureService.getCurrUser())
                .createDate(new Timestamp(System.currentTimeMillis()))
                .updateDate(new Timestamp(System.currentTimeMillis()))
                .build();
        return testService.saveAndFlush(test);
    }

    public Test updateTest(long testId, UpdateTestRequest updateTestRequest) {
        User user = secureService.getCurrUser();
        Test test = testService.getTestById(testId);
        if (test.getTestOwnerUser().equals(user)) {
            if (updateTestRequest.getName() != null) {
                test.setName(updateTestRequest.getName());
            }
            if (updateTestRequest.getDescription() != null) {
                test.setDescription(updateTestRequest.getDescription());
            }
            test.setUpdateDate(new Timestamp(System.currentTimeMillis()));
            return testService.saveAndFlush(test);

        } else {
            throw new AccessDeniedException();
        }
    }

    public List<Test> getAll() {
        User user = secureService.getCurrUser();
        return testService.getAllByOwnerUser(user);
    }

    public Test getById(long testId) {
        User user = secureService.getCurrUser();
        Test test = testService.getTestById(testId);
        if (test.getTestOwnerUser().equals(user)) {
            return test;
        }
        throw new AccessDeniedException("У вас нет доступа на редактирование этого теста");
    }

    public List<TestQuestion> getQuestionsByTest(long testId) {
        User user = secureService.getCurrUser();
        Test test = testService.getTestById(testId);
        if (test.getTestOwnerUser().equals(user)) {
            return testQuestionService.findAllByTest(test);
        }
        throw new AccessDeniedException("У вас нет доступа на редактирование этого теста");
    }

    public TestQuestion getQuestionById(long questionId) {
        User user = secureService.getCurrUser();
        TestQuestion question = testQuestionService.findById(questionId);
        if (question.getTest().getTestOwnerUser().equals(user)) {
            return question;
        }
        throw new AccessDeniedException("У вас нет доступа на редактирование этого вопроса");
    }

    public List<QuestionAnswer> getAnswers(TestQuestion question) {
        User user = secureService.getCurrUser();
        if (question.getTest().getTestOwnerUser().equals(user)) {
            return answersService.getAllByQuestion(question);
        }
        throw new AccessDeniedException("У вас нет доступа на редактирование этого вопроса");
    }

    @Transactional(rollbackOn = Exception.class)
    public TestQuestion addQuestion(long testId, QuestionRA questionRA) {
        User user = secureService.getCurrUser();
        Test test = testService.getTestById(testId);
        if (test.getTestOwnerUser().equals(user)) {
            TestQuestion question = TestQuestion.builder()
                    .text(questionRA.getQuestion().getText())
                    .multiAnswers(false)
                    .test(test)
                    .build();

            testQuestionService.saveAndFlush(question);
            updateAnswers(question, questionRA.getAnswers());
            return question;
        } else {
            throw new AccessDeniedException("У вас нет доступа на редактирование этого теста");
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public TestQuestion updateQuestion(long questionId, QuestionRA questionRA) {
        User user = secureService.getCurrUser();
        TestQuestion storedQuestion = testQuestionService.findById(questionId);
        if (storedQuestion.getTest().getTestOwnerUser().equals(user)) {
            storedQuestion.setText(questionRA.getQuestion().getText());
            storedQuestion.setMultiAnswers(false);

            testQuestionService.saveAndFlush(storedQuestion);
            updateAnswers(storedQuestion, questionRA.getAnswers());
            return storedQuestion;
        } else {
            throw new AccessDeniedException("У вас нет доступа на редактирование этого теста");
        }
    }

    private void updateAnswers(TestQuestion question, List<QuestionAnswer> answers) {
        List<QuestionAnswer> storedAnswers = answersService.getAllByQuestion(question);
        for (QuestionAnswer newAnswer : answers) {
            QuestionAnswer storedAnswer;
            if (newAnswer.getId() != null) {
                storedAnswer = storedAnswers.stream().filter(a -> a.getId()
                        .equals(newAnswer.getId()))
                        .findFirst()
                        .orElseThrow(() -> new AccessDeniedException("У вас нет доступа на редактирование этого ответа"));
            } else {
                storedAnswer = QuestionAnswer.builder()
                        .createDate(new Timestamp(System.currentTimeMillis()))
                        .question(question)
                        .build();
            }
            storedAnswer.setUpdateDate(new Timestamp(System.currentTimeMillis()));
            storedAnswer.setAnswerText(newAnswer.getAnswerText());
            storedAnswer.setRight(newAnswer.isRight());
            answersService.saveAndFlush(storedAnswer);
        }
    }


    public void deleteAnswer(long answerId) {
        User user = secureService.getCurrUser();
        QuestionAnswer answer = answersService.getById(answerId);
        if (answer.getQuestion().getTest().getTestOwnerUser().equals(user)){
            answersService.deleteAnswer(answer);
        } else {
            throw new AccessDeniedException();
        }
    }

    public void deleteTest(long testId) {
        User user = secureService.getCurrUser();
        Test test = testService.getTestById(testId);
        if (user.equals(test.getTestOwnerUser())) {
            testService.deleteTest(test);
        } else {
            throw new AccessDeniedException();
        }
    }

}
