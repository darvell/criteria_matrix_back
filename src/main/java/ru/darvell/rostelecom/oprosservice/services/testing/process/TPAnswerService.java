package ru.darvell.rostelecom.oprosservice.services.testing.process;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TPAnswer;
import ru.darvell.rostelecom.oprosservice.repos.testing.process.TestProcessAnswerRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class TPAnswerService {

    private final TestProcessAnswerRepository testProcessAnswerRepository;

    public void addAll(List<TPAnswer> answers) {
        testProcessAnswerRepository.saveAll(answers);
    }
}
