package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.OprosNotFoundException;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.model.Opros;
import ru.darvell.rostelecom.oprosservice.repos.OprosRepository;

import java.sql.Date;
import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class OprosService {

    private final OprosRepository oprosRepository;

    public Opros createOpros(CriteriaMatrix matrix) {
        Opros opros = new Opros();
        opros.setCreateDate(new Timestamp(System.currentTimeMillis()));
        opros.setOpen(true);
        opros.setCriteriaMatrix(matrix);
        return oprosRepository.saveAndFlush(opros);
    }

    public Opros getOprosById(long id) {
        return oprosRepository.findById(id).orElseThrow(OprosNotFoundException::new);
    }

    public Opros save(Opros opros) {
        return oprosRepository.saveAndFlush(opros);
    }
}
