package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.hibernate.service.spi.ServiceException;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.UserNotFoundException;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.UserRole;
import ru.darvell.rostelecom.oprosservice.repos.UserRepository;
import ru.darvell.rostelecom.oprosservice.repos.UserRoleRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class UsersService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    @Cacheable("users")
    public User findById(long userId) {
        return userRepository
                .findById(userId)
                .orElseThrow(UserNotFoundException::new);
    }

    @Cacheable("users")
    public User findByUserName(String userName) {
        return userRepository
                .findByUserName(userName)
                .orElse(new User());
    }

    Optional<User> checkUserCredentials(String userName, String password) {
        return userRepository
                .findByUserName(userName)
                .flatMap(user -> checkPassword(user, password));
    }

    private Optional<User> checkPassword(User user, String password) {
        return new BCryptPasswordEncoder().matches(password, user.getPassword()) ? Optional.of(user) : Optional.empty();
    }

    public List<User> getAllUsers() {
        List<User> users = userRepository.findAll();
        users.forEach(u -> u.setPassword(""));
        return users;
    }

    public List<UserRole> getPossibleRoles() {
        return userRoleRepository.findAll();
    }

    public Set<UserRole> getSimpleUserAuthorities() {
        UserRole role = userRoleRepository.findByRole("ROLE_USER");
        Set<UserRole> set = new HashSet<>();
        set.add(role);
        return set;
    }

    @Cacheable("users")
    public User addUser(User newUser) {
        User user = findByUserName(newUser.getUserName());
        if (user.getId() == null) {
            user.setUserName(newUser.getUserName());
            user.setPassword(new BCryptPasswordEncoder().encode(newUser.getPassword()));
            user.setEnabled(newUser.isEnabled());
//            user.setAuthorities(getPossibleRoles());
            user.setAccountNonExpired(true);
            user.setCredentialsNonExpired(true);
            user.setAccountNonLocked(true);
            return userRepository.saveAndFlush(user);
        } else {
            throw new ServiceException("Пользователь уже существует");
        }
    }

    @Cacheable("users")
    public User addUser(String login, String password) {
        User user = findByUserName(login);
        if (user.getId() == null) {
            user.setUserName(login);
            user.setPassword(new BCryptPasswordEncoder().encode(password));
            user.setEnabled(true);
            user.setAuthorities(getSimpleUserAuthorities());
            user.setAccountNonExpired(true);
            user.setCredentialsNonExpired(true);
            user.setAccountNonLocked(true);
            return userRepository.saveAndFlush(user);
        } else {
            throw new ServiceException("Пользователь уже существует");
        }
    }

    @CachePut(value = "users", key = "userId")
    public User updateUser(long userId, User newUser) {
        User user = findById(userId);
        if (newUser.getPassword() != null) {
            user.setPassword(new BCryptPasswordEncoder().encode(newUser.getPassword()));
        }
        user.setEnabled(newUser.isEnabled());
        user.setAuthorities(newUser.getAuthorities());
        return userRepository.saveAndFlush(user);
    }
}
