package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.InterviewNotFoundExceprtion;
import ru.darvell.rostelecom.oprosservice.model.Interviewee;
import ru.darvell.rostelecom.oprosservice.model.Opros;
import ru.darvell.rostelecom.oprosservice.repos.IntervieweeRepository;

@Service
@AllArgsConstructor
public class IntervieweeService {

    private final IntervieweeRepository intervieweeRepository;

    public Interviewee createInterview(Opros opros) {
        Interviewee interviewee = new Interviewee();
        interviewee.setOpros(opros);
        return intervieweeRepository.saveAndFlush(interviewee);
    }

    public Interviewee getById(long interviewId) {
        return intervieweeRepository.findById(interviewId).orElseThrow(InterviewNotFoundExceprtion::new);
    }

    public Interviewee save(Interviewee interviewee) {
        return intervieweeRepository.saveAndFlush(interviewee);
    }
}
