package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.darvell.rostelecom.oprosservice.exceptions.InterviewErrorException;
import ru.darvell.rostelecom.oprosservice.exceptions.OprosCloseException;
import ru.darvell.rostelecom.oprosservice.model.*;
import ru.darvell.rostelecom.oprosservice.web.models.Answer;
import ru.darvell.rostelecom.oprosservice.web.models.MatrixQuestionResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class IntervieweeProcessService {

    private final IntervieweeService intervieweeService;
    private final MatrixAdminService matrixAdminService;
    private final OprosService oprosService;

    public Interviewee joinOpros(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        if (opros.isOpen()) {
            return intervieweeService.createInterview(opros);
        } else {
            return null;
        }
    }

    public MatrixQuestionResponse getMatrixQuestionsByInterview(long interviewId) {
        Interviewee interviewee = intervieweeService.getById(interviewId);
        MatrixQuestionResponse mqr = getMatrixQuestionsByOpros(interviewee.getOpros().getId());
        //Убираем те критерии, на которые уже ответил
        if (interviewee.getAnswers() != null) {
            mqr.setCriterias(mqr.getCriterias().stream()
                    .filter(criteria -> !interviewee.getAnswers().containsKey(criteria.getId()))
                    .collect(Collectors.toList())
            );
        }
        mqr.setInterviewId(interviewId);
        return mqr;
    }

    public MatrixQuestionResponse getMatrixQuestionsByOpros(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        CriteriaMatrix criteriaMatrix = opros.getCriteriaMatrix();
        MatrixQuestionResponse questionResponse = new MatrixQuestionResponse();
        questionResponse.setCriterias(criteriaMatrix.getCriteriaSet().stream()
                .map(Criteria::genByExist)
                .collect(Collectors.toList())
        );
        questionResponse.setEntitys(criteriaMatrix.getMatrixEntities().stream()
                .map(MatrixEntity::genByExist)
                .collect(Collectors.toList())
        );
        questionResponse.setEntityGroups(criteriaMatrix.getEntityGroups());
        questionResponse.setNeedMax(criteriaMatrix.isNeedMax());
        questionResponse.setNeedMin(criteriaMatrix.isNeedMin());
        questionResponse.setPartAnswer(criteriaMatrix.isPartAnswer());
        questionResponse.setMaxRaiting(criteriaMatrix.getMaxRating());
        questionResponse.setActionText(criteriaMatrix.getActionText());
        questionResponse.setMaxAnswersCount(criteriaMatrix.getMaxAnswersCount());
        questionResponse.setCustom(criteriaMatrix.getCustom());

        return questionResponse;
    }

    public Interviewee setFIO(long interviewId, String f, String i, String o) {
        Interviewee interviewee = intervieweeService.getById(interviewId);
        if (interviewee.getOpros().isOpen()) {
            interviewee.setF(f);
            interviewee.setI(i);
            interviewee.setO(o);
            return intervieweeService.save(interviewee);
        } else {
            throw new OprosCloseException();
        }

    }

    public boolean canContinue(long interviewId){
        Interviewee interviewee = intervieweeService.getById(interviewId);
        if (!interviewee.getOpros().isOpen() || interviewee.getOpros().isDeleted()) {
            return false;
        }
        List<Criteria> criterias = interviewee.getOpros().getCriteriaMatrix().getCriteriaSet();
        if (interviewee.getAnswers() == null) {
            return true;
        }
        for (Criteria criteria : criterias) {
            if (!interviewee.getAnswers().containsKey(criteria.getId())) {
                return true;
            }
        }
        return false;
    }

    public boolean isOprosOpen(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        return opros.isOpen() && !opros.isDeleted();
    }

    @Transactional(rollbackFor = Throwable.class)
    public void publicAnswer(long interviewId, final long criteriaId, List<Answer> answers) {
        Interviewee interviewee = intervieweeService.getById(interviewId);
        Opros opros = interviewee.getOpros();
        CriteriaMatrix criteriaMatrix = opros.getCriteriaMatrix();
        criteriaMatrix.getCriteriaSet().stream().filter(c -> c.getId() == criteriaId).findFirst().orElseThrow(InterviewErrorException::new);


        if (opros.isOpen()) {

            Map<Long, Map<Long, Integer>> answersToAdd = interviewee.getAnswers();
            if (answersToAdd == null) {
                answersToAdd = new HashMap<>();
            }
            Map<Long, Integer> criteriaEntitys = new HashMap<>();

            if (criteriaMatrix.getMatrixEntities().size() != answers.stream().map(Answer::getEntityId).collect(Collectors.toSet()).size()) {
                throw new InterviewErrorException("Not all entitys present");
            }

            for (Answer answer : answers) {
                if (criteriaMatrix.getMatrixEntities().stream().anyMatch(e -> e.getId() == answer.getEntityId())) {
                    criteriaEntitys.put(answer.getEntityId(), answer.getAnswer());
                } else {
                    throw new InterviewErrorException();
                }
            }
            answersToAdd.put(criteriaId, criteriaEntitys);
            interviewee.setAnswers(answersToAdd);
            intervieweeService.save(interviewee);
        } else {
            throw new OprosCloseException();
        }
    }


}
