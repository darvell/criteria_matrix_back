package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.model.EntityGroup;
import ru.darvell.rostelecom.oprosservice.model.MatrixEntity;
import ru.darvell.rostelecom.oprosservice.repos.EntityGroupRepository;

import java.util.LinkedList;
import java.util.List;

@Service
@AllArgsConstructor
public class EntityGroupService {

    private final EntityGroupRepository repository;

    public EntityGroup createNewGroup(String name, CriteriaMatrix criteriaMatrix, List<MatrixEntity> entityes){
        EntityGroup entityGroup = new EntityGroup();
        entityGroup.setCriteriaMatrix(criteriaMatrix);
        entityGroup.setName(name);
        entityGroup.setEntityIds(new LinkedList<>());

        for (MatrixEntity entity: entityes) {
            entityGroup.getEntityIds().add(entity.getId());
        }
        return repository.saveAndFlush(entityGroup);
    }
}
