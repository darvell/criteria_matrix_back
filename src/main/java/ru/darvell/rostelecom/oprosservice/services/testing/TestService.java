package ru.darvell.rostelecom.oprosservice.services.testing;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.repos.testing.TestRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class TestService {

    private final TestRepository testRepository;

    public Test getTestById(long id){
        return testRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    List<Test> getAllByOwnerUser(User user) {
        return testRepository.getAllByTestOwnerUser(user);
    }

    Test saveAndFlush(Test test) {
        return testRepository.saveAndFlush(test);
    }

    void deleteTest(Test test) {
        test.setDeleted(true);
        saveAndFlush(test);
    }

}
