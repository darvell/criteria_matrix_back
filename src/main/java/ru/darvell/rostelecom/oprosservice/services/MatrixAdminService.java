package ru.darvell.rostelecom.oprosservice.services;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.AllArgsConstructor;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.darvell.rostelecom.oprosservice.exceptions.AccessDeniedException;
import ru.darvell.rostelecom.oprosservice.exceptions.BadCredentialsException;
import ru.darvell.rostelecom.oprosservice.exceptions.NoResultsException;
import ru.darvell.rostelecom.oprosservice.exceptions.QRCodeException;
import ru.darvell.rostelecom.oprosservice.model.*;
import ru.darvell.rostelecom.oprosservice.web.models.EntityGroupAddRequest;
import ru.darvell.rostelecom.oprosservice.web.models.MatrixAddRequest;
import ru.darvell.rostelecom.oprosservice.web.models.martixresult.ResultMatrixResponse;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class MatrixAdminService {

    private final MatrixService matrixService;
    private final CriteriaService criteriaService;
    private final SecureService secureService;
    private final MatrixEntityService entityService;
    private final OprosService oprosService;
    private final EntityGroupService entityGroupService;

    public CriteriaMatrix getByIdSecured(long matrixId) {
        User user = secureService.getCurrUser();
        return matrixService.getByIdAndUser(matrixId, user);
    }

    public List<CriteriaMatrix> getAllByUser() {
        User user = secureService.getCurrUser();
        return matrixService.getAllByUser(user);
    }

    public CriteriaMatrix createNewMatrix(String name, boolean useCoefficient) {
        User user = secureService.getCurrUser();
        CriteriaMatrix criteriaMatrix = new CriteriaMatrix();
        criteriaMatrix.setUser(user);
        criteriaMatrix.setName(name);
        criteriaMatrix.setUseCoefficient(useCoefficient);
        matrixService.saveAdnFlush(criteriaMatrix);
        oprosService.createOpros(criteriaMatrix);
        return criteriaMatrix;
    }

    @Transactional(rollbackFor = Exception.class)
    public CriteriaMatrix createNewMatrix(MatrixAddRequest request) {
        User user = secureService.getCurrUser();
        CriteriaMatrix criteriaMatrix = new CriteriaMatrix();
        criteriaMatrix.setUser(user);
        criteriaMatrix.setName(request.getName());
        criteriaMatrix.setUseCoefficient(request.isUseCoefficient());
        criteriaMatrix = matrixService.saveAdnFlush(criteriaMatrix);

        addCriterias(criteriaMatrix.getId(), request.getCriteriaList());

        addEntitys(criteriaMatrix, request.getEntityList());


        oprosService.createOpros(criteriaMatrix);

        return getByIdSecured(criteriaMatrix.getId());
    }

    public CriteriaMatrix updateMatrix(long matrixId, String name, Boolean useCoefficient) {
        CriteriaMatrix criteriaMatrix = getByIdSecured(matrixId);
        if (name != null) {
            criteriaMatrix.setName(name);
        }
        if (useCoefficient != null) {
            criteriaMatrix.setUseCoefficient(useCoefficient);
        }
        return matrixService.saveAdnFlush(criteriaMatrix);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deleteMatrix(long matrixId) {
        CriteriaMatrix criteriaMatrix = getByIdSecured(matrixId);
        criteriaMatrix.setDeleted(true);
        matrixService.saveAdnFlush(criteriaMatrix);

        for (Opros opros: criteriaMatrix.getOprosList()) {
            opros.setDeleted(true);
            oprosService.save(opros);
        }
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public CriteriaMatrix addCriterias(long matrixId, List<Criteria> criteriaList) {
        CriteriaMatrix criteriaMatrix = getByIdSecured(matrixId);
        for (Criteria criteria : criteriaList) {
            criteria.setId(null);
            criteria.setCriteriaMatrix(criteriaMatrix);
            criteriaService.saveCriteria(criteria);
        }
        return getByIdSecured(matrixId);
    }

    public Criteria addCriteria(long matrixId, String value, Double weight){
        CriteriaMatrix criteriaMatrix = getByIdSecured(matrixId);
        Criteria criteria = new Criteria();
        criteria.setValue(value);
        if (weight != null) {
            criteria.setWeight(weight);
        }
        criteria.setCriteriaMatrix(criteriaMatrix);
        return criteriaService.saveCriteria(criteria);
    }

    public MatrixEntity addEntity(long matrixId, String value) {
        CriteriaMatrix criteriaMatrix = getByIdSecured(matrixId);
        MatrixEntity entity = new MatrixEntity();
        entity.setValue(value);
        entity.setCriteriaMatrix(criteriaMatrix);
        return entityService.saveMatrixEntity(entity);
    }

    public boolean updateCriteriaValue(long criteriaId, String value, Double weight) {
        Criteria criteria = criteriaService.findById(criteriaId);
        User user = secureService.getCurrUser();
        if (criteria.getCriteriaMatrix().getUser().getId().equals(user.getId())) {
            criteria.setValue(value);
            if (weight != null) {
                criteria.setWeight(weight);
            }
            criteriaService.saveCriteria(criteria);
            return true;
        }
        return false;
    }

    public boolean updateMatrixEntityValue(long entityId, String value) {
        MatrixEntity entity = entityService.findById(entityId);
        User user = secureService.getCurrUser();
        if (entity.getCriteriaMatrix().getUser().getId().equals(user.getId())) {
            entity.setValue(value);
            entityService.saveMatrixEntity(entity);
            return true;
        }
        return false;
    }

    @Transactional(rollbackFor = Exception.class)
    public EntityGroup addEntityGroup(long matrixId, EntityGroupAddRequest request) {
        CriteriaMatrix matrix = getByIdSecured(matrixId);
        List<MatrixEntity> entities = new LinkedList<>();
        for(long entId: request.getEntityIds()) {
            entities.add(entityService.findByIdAndCriteriaMatrix(entId, matrix));
        }
        return entityGroupService.createNewGroup(request.getName(), matrix, entities);
    }

    @Transactional(rollbackFor = Exception.class)
    public CriteriaMatrix addEntitys(long matrixId, List<MatrixEntity> entityList) {
        CriteriaMatrix criteriaMatrix = getByIdSecured(matrixId);
        return addEntitys(criteriaMatrix, entityList);
    }

    private CriteriaMatrix addEntitys(CriteriaMatrix criteriaMatrix, List<MatrixEntity> entityList) {
        for (MatrixEntity entity : entityList) {
            entity.setId(null);
            entity.setCriteriaMatrix(criteriaMatrix);
            entityService.saveMatrixEntity(entity);
        }
        return getByIdSecured(criteriaMatrix.getId());
    }

    public CriteriaMatrix stopOpros(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        User user = secureService.getCurrUser();
        CriteriaMatrix criteriaMatrix = matrixService.getByIdAndUser(opros.getCriteriaMatrix().getId(), user);
//        CriteriaMatrix criteriaMatrix = opros.getCriteriaMatrix();
        if (criteriaMatrix.getUser().equals(user)) {
            opros.setOpen(false);
            oprosService.save(opros);
        } else {
            throw new AccessDeniedException();
        }
        return matrixService.getByIdAndUser(criteriaMatrix.getId(), user);
    }

    public CriteriaMatrix deleteOpros(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        User user = secureService.getCurrUser();
        if (opros.getCriteriaMatrix().getUser().equals(user)) {
            opros.setDeleted(true);
            opros.setOpen(false);
            oprosService.save(opros);
        } else {
            throw new AccessDeniedException();
        }

        return matrixService.getByIdAndUser(opros.getCriteriaMatrix().getId(), user);
    }

    public long calculateInterviewCount(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        return opros.getInterviewees().stream()
                .filter(i -> i.getAnswers() != null).count();
    }

    public CriteriaMatrix newOpros(long matrixId) {
        CriteriaMatrix criteriaMatrix = getByIdSecured(matrixId);
        Opros opros = new Opros();
        opros.setCreateDate(new Timestamp(System.currentTimeMillis()));
        opros.setOpen(true);
        opros.setCriteriaMatrix(criteriaMatrix);
        opros.setInterviewees(new ArrayList<>());
        oprosService.save(opros);
        return criteriaMatrix;
    }

    public BufferedImage genQrCode(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        StringBuilder sb = new StringBuilder();
//        sb.append("http://localhost:4200/opros/join/");
        sb.append("https://www.matrix.darvell.ru/opros/join/" + oprosId);
//        sb.append(opros.getId());

        QRCodeWriter barcodeWriter = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = barcodeWriter.encode(sb.toString(), BarcodeFormat.QR_CODE, 400, 400);
            return MatrixToImageWriter.toBufferedImage(bitMatrix);
        } catch (WriterException e) {
            throw new QRCodeException();
        }
    }



    public ByteArrayInputStream generateExcell(long oprosId) {

        ResultMatrixResponse rmResponse = calculateMatrixByOprosId(oprosId);
        List<EntityGroup> entityGroups = oprosService.getOprosById(oprosId).getCriteriaMatrix().getEntityGroups();
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Результирующая матрица");

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(font);

        Row header = sheet.createRow(0);
        header.createCell(0).setCellStyle(headerStyle);
        header.getCell(0).setCellValue("Сущность");


        int i = 1;
        for (Long critId : new TreeSet<>(rmResponse.getCriteriaLegend().keySet())) {
            header.createCell(i).setCellStyle(headerStyle);
            header.getCell(i).setCellValue(rmResponse.getCriteriaLegend().get(critId));
            i++;
        }
        header.createCell(i).setCellStyle(headerStyle);
        header.getCell(i).setCellValue("Комплексная оценка");


        CellStyle dataStyle = workbook.createCellStyle();
        dataStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat("0.00"));

        Font groupFont = workbook.createFont();
        groupFont.setFontName("Arial");
        groupFont.setBold(true);
        groupFont.setColor(Font.COLOR_RED);
        CellStyle groupStyle = workbook.createCellStyle();
        groupStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat("0.00"));
        groupStyle.setFont(groupFont);

        int rowNumber = 1;
        if (entityGroups.size() > 0) {
            List<Long> entIdsInGroup = new LinkedList<>();
            for (EntityGroup entityGroup: entityGroups) {
                Row dataRow = sheet.createRow(rowNumber);
                rowNumber++;
                Cell gCell = dataRow.createCell(0);
                gCell.setCellValue(entityGroup.getName());
                gCell.setCellStyle(groupStyle);
                for (long entId : entityGroup.getEntityIds()) {
                    entIdsInGroup.add(entId);
                    fillEntityRowMasterSheet(sheet, dataStyle, rowNumber, rmResponse, entId);
                    rowNumber++;
                }
            }

            Row dataRow = sheet.createRow(rowNumber);
            rowNumber++;
            Cell gCell = dataRow.createCell(0);
            gCell.setCellValue("Без группы");
            gCell.setCellStyle(groupStyle);
            for (long entId: rmResponse.getEntityLegend().keySet()) {
                if (!entIdsInGroup.contains(entId)){
                    fillEntityRowMasterSheet(sheet, dataStyle, rowNumber, rmResponse, entId);
                    rowNumber++;
                }
            }

        } else {


            for (long entId : rmResponse.getEntityLegend().keySet()) {
                fillEntityRowMasterSheet(sheet, dataStyle, rowNumber, rmResponse, entId);
                rowNumber++;
            }
        }

        for (int t = 0; t <= i; t++) {
            sheet.autoSizeColumn(t);
        }

        Opros opros = oprosService.getOprosById(oprosId);
        List<Interviewee> interviewees = opros.getInterviewees();
        clearNotFullInterview(interviewees);
        int iFroName = 1;
        for (Interviewee interviewee : interviewees) {
            Map<Long, Map<Long, Integer>> answers = transpondeInterviewResult(interviewee.getAnswers(), rmResponse.getEntityLegend().keySet());
            Sheet sheetInt = workbook.createSheet(iFroName + "-" + interviewee.getF() + " " + interviewee.getI());
            Row headerInt = sheetInt.createRow(0);
            headerInt.createCell(0).setCellStyle(headerStyle);
            headerInt.getCell(0).setCellValue("Критерий");

            i = 1;
            for (Long critId : new TreeSet<>(rmResponse.getCriteriaLegend().keySet())) {
                headerInt.createCell(i).setCellStyle(headerStyle);
                headerInt.getCell(i).setCellValue(rmResponse.getCriteriaLegend().get(critId));
                i++;
            }

            rowNumber = 1;
            for (long entityId : rmResponse.getEntityLegend().keySet()) {
                Row dataRowInt = sheetInt.createRow(rowNumber);
                dataRowInt.createCell(0).setCellValue(rmResponse.getEntityLegend().get(entityId));
                int j = 1;

                Map<Long, Integer> entityRow = answers.get(entityId);
                for (Long critId : new TreeSet<>(entityRow.keySet())) {
                    Integer sum = entityRow.get(critId);
                    if (sum == null) {sum = 0;}
                    dataRowInt.createCell(j).setCellStyle(dataStyle);
                    dataRowInt.getCell(j).setCellValue(sum);
                    j++;
                }
                rowNumber++;
            }
            for (int t = 0; t <= i; t++) {
                sheetInt.autoSizeColumn(t);
            }
            iFroName++;

        }


        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void fillEntityRowMasterSheet(Sheet sheet, CellStyle dataStyle, int rowNumber, ResultMatrixResponse rmResponse, long entId){
        Row dataRow = sheet.createRow(rowNumber);
        dataRow.createCell(0).setCellValue(rmResponse.getEntityLegend().get(entId));
        int j = 1;

        double complexSumm = 0d;
        Map<Long, Double> entityRow = rmResponse.getResultMatrix().get(entId);
        for (Long critId : new TreeSet<>(entityRow.keySet())) {
            double sum = entityRow.get(critId);
            if (rmResponse.getWeights() != null) {
                complexSumm += rmResponse.getWeights().get(critId) * sum;
            } else {
                complexSumm += sum;
            }
            dataRow.createCell(j).setCellStyle(dataStyle);
            dataRow.getCell(j).setCellValue(sum);
            j++;
        }
        dataRow.createCell(j).setCellValue(complexSumm);
        dataRow.getCell(j).setCellStyle(dataStyle);
    }

    public ResultMatrixResponse calculateMatrixByOprosId(long oprosId) {
        Opros opros = oprosService.getOprosById(oprosId);
        User user = secureService.getCurrUser();
        CriteriaMatrix criteriaMatrix = matrixService.getByIdAndUser(opros.getCriteriaMatrix().getId(), user);

        Map<Long, Map<Long, Double>> result = new HashMap<>();
        for (Criteria criteria : criteriaMatrix.getCriteriaSet()) {
            Map<Long, Double> row = new HashMap<>();
            for (MatrixEntity entity : criteriaMatrix.getMatrixEntities()) {
                row.put(entity.getId(), 0d);
            }
            result.put(criteria.getId(), row);
        }
        List<Interviewee> interviewees = opros.getInterviewees();

        clearNotFullInterview(interviewees);

        if (interviewees.size() == 0) {
            throw new NoResultsException();
        }
        int intrewiewCount = interviewees.size();
        for (Interviewee interviewee : interviewees) {
            Map<Long, Map<Long, Integer>> answers = interviewee.getAnswers();
            for (long cId : answers.keySet()) {
                Map<Long, Double> editRow = result.get(cId);
                Map<Long, Integer> answerRow = answers.get(cId);
                for (long eId : answerRow.keySet()) {
                    editRow.put(eId, editRow.get(eId) + answerRow.get(eId));
                }
            }
        }

        for (long cId : result.keySet()) {
            for (long eId : result.get(cId).keySet()) {
                result.get(cId).put(eId,
                        result.get(cId).get(eId) / intrewiewCount
                );
                System.out.print(result.get(cId).get(eId));
                System.out.print("  ");
            }
            System.out.println();
        }

        ResultMatrixResponse resultMatrixResponse = new ResultMatrixResponse();
        resultMatrixResponse.setResultMatrix(transpondeResult(result, criteriaMatrix.getMatrixEntities().stream().map(MatrixEntity::getId).collect(Collectors.toSet())));
        resultMatrixResponse.setCriteriaLegend(criteriaMatrix.getCriteriaSet().stream().collect(Collectors.toMap(Criteria::getId, Criteria::getValue)));
        resultMatrixResponse.setEntityLegend(criteriaMatrix.getMatrixEntities().stream().collect(Collectors.toMap(MatrixEntity::getId, MatrixEntity::getValue)));
        if (criteriaMatrix.isUseCoefficient()) {
            resultMatrixResponse.setWeights(criteriaMatrix.getCriteriaSet().stream().collect(Collectors.toMap(Criteria::getId, Criteria::getWeight)));
        }
        return resultMatrixResponse;
    }

    private Map<Long, Map<Long, Double>> transpondeResult(Map<Long, Map<Long, Double>> result, Set<Long> matrixEntityIds) {
        Map<Long, Map<Long, Double>> transponded = new HashMap<>();
        for (long entId : matrixEntityIds) {
            Map<Long, Double> map = new HashMap<>();
            for (long critId : result.keySet()) {
                map.put(critId, null);
            }
            transponded.put(entId, map);
        }
        for (long critId : result.keySet()) {
            Map<Long, Double> entitys = result.get(critId);
            for (long entityId : entitys.keySet()) {
                transponded.get(entityId).put(critId, entitys.get(entityId));
            }
        }
        return transponded;
    }

    private Map<Long, Map<Long, Integer>> transpondeInterviewResult(Map<Long, Map<Long, Integer>> result, Set<Long> matrixEntityIds) {
        Map<Long, Map<Long, Integer>> transponded = new HashMap<>();
        for (long entId : matrixEntityIds) {
            Map<Long, Integer> map = new HashMap<>();
            for (long critId : result.keySet()) {
                map.put(critId, null);
            }
            transponded.put(entId, map);
        }
        for (long critId : result.keySet()) {
            Map<Long, Integer> entitys = result.get(critId);
            for (long entityId : entitys.keySet()) {
                transponded.get(entityId).put(critId, entitys.get(entityId));
            }
        }
        return transponded;
    }

    private void clearNotFullInterview(List<Interviewee> interviewees) {
        Iterator<Interviewee> intervieweeIterator = interviewees.iterator();
        while (intervieweeIterator.hasNext()) {
            Interviewee interviewee = intervieweeIterator.next();
            Map<Long, Map<Long, Integer>> answers = interviewee.getAnswers();
            if (answers == null) {
                intervieweeIterator.remove();
                System.out.println("remove interview id " + interviewee.getId());
            }

        }

    }

}
