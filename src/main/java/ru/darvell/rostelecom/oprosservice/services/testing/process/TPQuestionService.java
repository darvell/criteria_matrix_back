package ru.darvell.rostelecom.oprosservice.services.testing.process;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TPQuestion;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TestProcess;
import ru.darvell.rostelecom.oprosservice.repos.testing.process.TPQuestionRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class TPQuestionService {

    private final TPQuestionRepository questionRepository;

    public void saveAndFlush(TPQuestion tpQuestion){
        questionRepository.saveAndFlush(tpQuestion);
    }

    public List<TPQuestion> findAllByTestProcess(TestProcess testProcess){
        return questionRepository.findAllByTestProcess(testProcess);
    }

    public TPQuestion findAllTyTestProcessAndId(TestProcess testProcess, long id) throws NotFoundException {
        return questionRepository.findByTestProcessAndId(testProcess, id)
                .orElseThrow(()->new NotFoundException("Question of test process not find"));
    }
}
