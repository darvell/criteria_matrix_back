package ru.darvell.rostelecom.oprosservice.services.testing.process;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TestProcess;
import ru.darvell.rostelecom.oprosservice.repos.testing.process.TestProcessRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class TestProcessService {

    private final TestProcessRepository testProcessRepository;

    TestProcess saveAndFlush(TestProcess testProcess) {
        return testProcessRepository.saveAndFlush(testProcess);
    }

    TestProcess findById(long id) {
        return testProcessRepository.findById(id).orElseThrow(() -> new NotFoundException("Test process not finded"));
    }

    List<TestProcess> findAllByTestAndUserId(Test test, Long userId) {
        return testProcessRepository.findAllByTestAndUserId(test, userId);
    }

    List<TestProcess> findAllByUserId(Long userId) {
        return testProcessRepository.findAllByUserId(userId);
    }

}
