package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.Criteria;
import ru.darvell.rostelecom.oprosservice.repos.CriteriaRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class CriteriaService {

    private final CriteriaRepository criteriaRepository;

    public List<Criteria> addAllCriteria(List<Criteria> criteriaList) {
        return criteriaRepository.saveAll(criteriaList);
    }

    public Criteria saveCriteria(Criteria criteria) {
        return criteriaRepository.saveAndFlush(criteria);
    }

    public Criteria findById(long id) {
        return criteriaRepository.findById(id).orElseThrow(NotFoundException::new);
    }



}
