package ru.darvell.rostelecom.oprosservice.services.testing.process;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.AccessDeniedException;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.testing.QuestionAnswer;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;
import ru.darvell.rostelecom.oprosservice.model.testing.TestQuestion;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TPAnswer;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TPQuestion;
import ru.darvell.rostelecom.oprosservice.model.testing.process.TestProcess;
import ru.darvell.rostelecom.oprosservice.services.SecureService;
import ru.darvell.rostelecom.oprosservice.services.testing.AnswersService;
import ru.darvell.rostelecom.oprosservice.services.testing.TestQuestionService;
import ru.darvell.rostelecom.oprosservice.services.testing.TestService;
import ru.darvell.rostelecom.oprosservice.web.models.testing.user.Answer;
import ru.darvell.rostelecom.oprosservice.web.models.testing.user.Question;
import ru.darvell.rostelecom.oprosservice.web.models.testing.user.TestInAction;
import ru.darvell.rostelecom.oprosservice.web.models.testing.user.TestResponse;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserTestService {

    private final TestProcessService testProcessService;
    private final SecureService secureService;
    private final TestService testService;
    private final TestQuestionService testQuestionService;
    private final AnswersService answersService;
    private final TPQuestionService tpQuestionService;
    private final TPAnswerService tpAnswerService;


    public TestInAction fillTestInAction(long testId) {
        Test test = testService.getTestById(testId);
        if (!test.isDeleted()) {
            return new TestInAction(test);
        }
        return null;
    }

    public List<TestProcess> getActiveProcessByTestId(long testId) {
        User user = secureService.getCurrUser();
        Test test = testService.getTestById(testId);
        return testProcessService.findAllByTestAndUserId(test, user.getId());
    }

    public List<TestProcess> getActiveProcessByCurrUser() {
        User user = secureService.getCurrUser();
        return testProcessService.findAllByUserId(user.getId());
    }

    @Transactional(rollbackOn = Exception.class)
    public TestProcess joinToTest(long testId) {
        User user = secureService.getCurrUser();
        Test test = testService.getTestById(testId);

        TestProcess testProcess = TestProcess.builder()
                .completed(false)
                .createDate(new Timestamp(System.currentTimeMillis()))
                .user(user)
                .test(test)
                .build();

        testProcessService.saveAndFlush(testProcess);

        prepareQuestions(test, testProcess);

        return testProcess;
    }

    private void prepareQuestions(Test test, TestProcess testProcess) {
        List<TestQuestion> questionList = testQuestionService.findAllByTest(test);
        for (TestQuestion question : questionList) {
            TPQuestion tpQuestion = new TPQuestion();
            tpQuestion.setCompleted(false);
            tpQuestion.setQuestionId(question.getId());
            tpQuestion.setTestProcess(testProcess);
            tpQuestionService.saveAndFlush(tpQuestion);
        }
    }

    public TestResponse collectDataForTest(long testProcessId) {
        User user = secureService.getCurrUser();
        TestResponse testResponse = new TestResponse();
        TestProcess testProcess = testProcessService.findById(testProcessId);
        if (!testProcess.getUser().equals(user)) {
            throw new AccessDeniedException();
        }
        for (TPQuestion tpQuestion : tpQuestionService.findAllByTestProcess(testProcess)) {
            if (!tpQuestion.isCompleted()) {
                Question questionResp = new Question();
                TestQuestion testQuestion = testQuestionService.findById(tpQuestion.getQuestionId());
                questionResp.setText(testQuestion.getText());
                questionResp.setId(tpQuestion.getId());

                List<QuestionAnswer> questionAnswers = answersService.getAllByQuestion(testQuestion);
                for (QuestionAnswer questionAnswer : questionAnswers.stream()
                        .filter(answer -> !answer.isDeleted())
                        .collect(Collectors.toList())) {
                    Answer answerResp = new Answer();
                    answerResp.setId(questionAnswer.getId());
                    answerResp.setText(questionAnswer.getAnswerText());

                    questionResp.addAnswer(answerResp);
                }
                testResponse.addQuestion(questionResp);
            }
        }
        return testResponse;
    }

    @Transactional(rollbackOn = Exception.class)
    public boolean addAnswer(long testProcessId, long tpQuestionId, List<Long> answers) throws AccessDeniedException {
        User user = secureService.getCurrUser();
        TestProcess testProcess = testProcessService.findById(testProcessId);
        if (!testProcess.getUser().equals(user)) {
            throw new AccessDeniedException("You cannot access this test process");
        }
        TPQuestion tpQuestion = tpQuestionService.findAllTyTestProcessAndId(testProcess, tpQuestionId);
        if (tpQuestion.isCompleted()) {
            return false;
        }
        tpQuestion.setCompleted(true);

        TestQuestion question = testQuestionService.findById(tpQuestion.getQuestionId());
        List<QuestionAnswer> questionAnswers = answersService.getAllByQuestion(question);

        List<TPAnswer> rightAnswers = new LinkedList<>();

        for (long answerId : questionAnswers.stream()
                .map(QuestionAnswer::getId)
                .collect(Collectors.toList())) {
            if (answers.contains(answerId)) {
                rightAnswers.add(TPAnswer.builder()
                        .answerId(answerId)
                        .tpTestQuestion(tpQuestion)
                        .build());
            }
        }
        tpQuestionService.saveAndFlush(tpQuestion);
        tpAnswerService.addAll(rightAnswers);

        return true;
    }

    public void finishTest(long testProcessId) {
        TestProcess testProcess = testProcessService.findById(testProcessId);
        User currUser = secureService.getCurrUser();
        User testUser = testProcess.getUser();
        User testOwner = testProcess.getTest().getTestOwnerUser();
        if (currUser.equals(testUser) || currUser.equals(testOwner)) {
            testProcess.setCompleted(true);
            testProcessService.saveAndFlush(testProcess);
        } else {
            throw new AccessDeniedException();
        }
    }
}
