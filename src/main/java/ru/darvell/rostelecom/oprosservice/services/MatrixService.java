package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.darvell.rostelecom.oprosservice.exceptions.MatrixNotFoundException;
import ru.darvell.rostelecom.oprosservice.model.Criteria;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.repos.CriteriaMatrixRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class MatrixService {

    private final CriteriaMatrixRepository criteriaMatrixRepository;

    public CriteriaMatrix getByIdAndUser(long id, User user) {
        return criteriaMatrixRepository.findByIdAndUser(id, user).orElseThrow(MatrixNotFoundException::new);
    }

    public List<CriteriaMatrix> getAllByUser(User user) {
        return criteriaMatrixRepository.getAllByUser(user);
    }

    public CriteriaMatrix saveAdnFlush(CriteriaMatrix matrix) {
        criteriaMatrixRepository.saveAndFlush(matrix);
        return matrix;
    }



}
