package ru.darvell.rostelecom.oprosservice.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.darvell.rostelecom.oprosservice.exceptions.NotFoundException;
import ru.darvell.rostelecom.oprosservice.model.Criteria;
import ru.darvell.rostelecom.oprosservice.model.CriteriaMatrix;
import ru.darvell.rostelecom.oprosservice.model.MatrixEntity;
import ru.darvell.rostelecom.oprosservice.repos.MatrixEntityRepository;

@Service
@AllArgsConstructor
public class MatrixEntityService {

    private final MatrixEntityRepository matrixEntityRepository;

    public MatrixEntity saveMatrixEntity(MatrixEntity matrixEntity) {
        return matrixEntityRepository.saveAndFlush(matrixEntity);
    }

    public MatrixEntity findById(long id) {
        return matrixEntityRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public MatrixEntity findByIdAndCriteriaMatrix(long id, CriteriaMatrix criteriaMatrix) {
        return matrixEntityRepository.findByIdAndCriteriaMatrix(id, criteriaMatrix).orElseThrow(NotFoundException::new);
    }
}
