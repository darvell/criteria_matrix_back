package ru.darvell.rostelecom.oprosservice.config;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.darvell.rostelecom.oprosservice.services.TokenAuthService;
import ru.darvell.rostelecom.oprosservice.web.StatelessAuthFilter;

@EnableWebSecurity
@AllArgsConstructor
public class SecurtyConfig extends WebSecurityConfigurerAdapter {

    private TokenAuthService tokenAuthService;

    private MyAuthenticationEntryPoint myAuthenticationEntryPoint;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//                .cors().and()
                .httpBasic().disable()
                .csrf().disable()
                .addFilterBefore(new StatelessAuthFilter(tokenAuthService), UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling().authenticationEntryPoint(myAuthenticationEntryPoint)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
//                .antMatchers(HttpMethod.POST, "/auth").permitAll()
//                .antMatchers("/eureka-dash").permitAll()
//                .antMatchers("/eureka/**").permitAll()
//                .antMatchers("/token/check").permitAll()
//                .antMatchers("/api/billmail/**").hasRole("BILL_MAIL")
//                .antMatchers("/**").hasRole("USER")
//                .antMatchers("/**").permitAll()
                .antMatchers("/auth").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/token/check").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/actuator/**").permitAll()
                .antMatchers("/matrixAdmin/**").hasRole("USER")
                .antMatchers("/test").permitAll()
                .antMatchers("/opros/**").hasRole("USER")
                .antMatchers("/interview/**").permitAll()
                .antMatchers("/qrcode/**").permitAll()
                .antMatchers("/testing/**").hasRole("USER")
                .antMatchers("/do_test/**").permitAll()
                .anyRequest().authenticated();
//                .and()
//                .logoutSuccessHandler(new CustomLogoutSuccessHandler(tokenAuthService))
//                .permitAll();
    }



}
