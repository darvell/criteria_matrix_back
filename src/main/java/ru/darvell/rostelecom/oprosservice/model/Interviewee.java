package ru.darvell.rostelecom.oprosservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "interviewee")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
@ToString
@TypeDef(name = "JsonObject", typeClass = JsonStringType.class)
public class Interviewee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "f")
    private String f;

    @Column(name = "i")
    private String i;

    @Column(name = "o")
    private String o;

    @Column(name = "answers")
    @Type(type = "JsonObject")
    private Map<Long, Map<Long, Integer>> answers;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "opros_id")
    @JsonIgnore
    private Opros opros;
}
