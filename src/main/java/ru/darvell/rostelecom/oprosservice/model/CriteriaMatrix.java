package ru.darvell.rostelecom.oprosservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Матрица критериев
 */
@Entity
@Table(name = "criteria_matrix")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
//@ToString
public class CriteriaMatrix {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Владелец матрицы
     */
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "opros_user_id")
    private User user;

    /**
     * Максимальное количество опрашиваемых
     */
    @Column(name = "max_member_count")
    private int maxMembersCount;

    /**
     * Использовать ли весовые коэфициенты
     */
    @Column(name = "use_coefficient")
    private boolean useCoefficient;

    /**
     * Ссылка для qr кода на регистрацию
     */
    @Column(name = "qr_link")
    private String qrLink;

    @Column(name = "name")
    private String name;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "need_max")
    private boolean needMax = true;

    @Column(name = "need_min")
    private boolean needMin = true;

    @Column(name = "part_answer")
    private boolean partAnswer = false;

    @Column(name = "max_rating")
    private int maxRating = 10;

    @Column(name = "action_text")
    private String actionText;

    @Column(name = "max_answers_count")
    private int maxAnswersCount;

    @Column(name = "custom")
    private int custom;

    /**
     * Список критерив
     */
    @OneToMany(mappedBy = "criteriaMatrix", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Criteria> criteriaSet;

    /**
     * Список сушностей
     */
    @OneToMany(mappedBy = "criteriaMatrix", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MatrixEntity> matrixEntities;

    @OneToMany(mappedBy = "criteriaMatrix", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Opros> oprosList;

    @OneToMany(mappedBy = "criteriaMatrix", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<EntityGroup> entityGroups;

}
