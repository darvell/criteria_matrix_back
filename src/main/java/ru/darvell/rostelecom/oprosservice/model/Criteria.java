package ru.darvell.rostelecom.oprosservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "criteria")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Criteria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "value")
    private String value;

    @Column(name = "weight")
    private Double weight;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "criteria_matrix_id")
    @JsonIgnore
    private CriteriaMatrix criteriaMatrix;

    public static Criteria genByExist(Criteria criteria) {
        Criteria c = new Criteria();
        c.setId(criteria.getId());
        c.setValue(criteria.getValue());
        return c;
    }

}
