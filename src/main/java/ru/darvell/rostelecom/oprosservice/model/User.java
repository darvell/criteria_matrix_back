package ru.darvell.rostelecom.oprosservice.model;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "opros_user")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name")
    private String userName;
    private String password;

    private boolean enabled;

    @Column(name = "account_non_expired")
    private boolean accountNonExpired;

    @Column(name = "account_non_locked")
    private boolean accountNonLocked;

    @Column(name = "credentials_non_expired")
    private boolean credentialsNonExpired;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "opros_user_role",
            joinColumns = @JoinColumn(name = "opros_user_id"),
            inverseJoinColumns = @JoinColumn(name = "opros_role_id")
    )
    private Set<UserRole> authorities;
}
