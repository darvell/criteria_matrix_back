package ru.darvell.rostelecom.oprosservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "entity_group")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
@TypeDef(name = "JsonObject", typeClass = JsonStringType.class)
public class EntityGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "criteria_matrix_id")
    @JsonIgnore
    private CriteriaMatrix criteriaMatrix;

    @Column(name = "entity_ids")
    @Type(type = "JsonObject")
    private List<Long> entityIds;


}
