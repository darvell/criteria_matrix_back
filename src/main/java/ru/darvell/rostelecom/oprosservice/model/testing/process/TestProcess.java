package ru.darvell.rostelecom.oprosservice.model.testing.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import ru.darvell.rostelecom.oprosservice.model.User;
import ru.darvell.rostelecom.oprosservice.model.testing.Test;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "test_process")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
@DynamicUpdate
@Builder
public class TestProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "test_id")
    @JsonIgnore
    private Test test;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Column(name = "create_date")
    private Timestamp createDate;

    @Column(name = "complete_date")
    private Timestamp completeDate;

    @Column(name = "completed")
    private boolean completed;

    @Column(name = "deleted")
    private boolean deleted;
}
