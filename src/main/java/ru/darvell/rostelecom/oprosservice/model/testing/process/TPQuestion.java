package ru.darvell.rostelecom.oprosservice.model.testing.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "tp_question")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
@DynamicUpdate
@Builder
public class TPQuestion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "test_process_id")
    @JsonIgnore
    private TestProcess testProcess;

    @Column(name = "question_id")
    private Long questionId;

    @Column(name = "completed")
    private boolean completed;
}
