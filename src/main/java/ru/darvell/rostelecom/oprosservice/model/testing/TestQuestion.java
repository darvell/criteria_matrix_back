package ru.darvell.rostelecom.oprosservice.model.testing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "test_question")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
@DynamicUpdate
@Builder
public class TestQuestion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "test_id")
    @JsonIgnore
    private Test test;

    @Column(name = "create_date")
    private Timestamp createDate;

    @Column(name = "update_date")
    private Timestamp updateDate;

    @Column(name = "question_text")
    private String text;

    @JsonIgnore
    @Column(name = "image_id")
    private String imageId;

    @Column(name = "mult_answers")
    private boolean multiAnswers;

    @Column(name = "sort_order")
    private int sortOrder;

}
