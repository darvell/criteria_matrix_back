package ru.darvell.rostelecom.oprosservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "opros")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Opros {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "create_date")
    private Timestamp createDate;

    @Column(name = "open")
    private boolean open;

    @Column(name = "deleted")
    private boolean deleted;

    @OneToMany(mappedBy = "opros", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Interviewee> interviewees;

    @Transient
    private int intervieweesCount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "criteria_matrix_id")
    @JsonIgnore
    private CriteriaMatrix criteriaMatrix;

}
