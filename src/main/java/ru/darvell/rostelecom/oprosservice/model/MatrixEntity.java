package ru.darvell.rostelecom.oprosservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "matrix_entity")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class MatrixEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "criteria_matrix_id")
    @JsonIgnore
    private CriteriaMatrix criteriaMatrix;

    public static MatrixEntity genByExist(MatrixEntity entity) {
        MatrixEntity e = new MatrixEntity();
        e.setId(entity.id);
        e.setValue(entity.value);
        return e;
    }
}
