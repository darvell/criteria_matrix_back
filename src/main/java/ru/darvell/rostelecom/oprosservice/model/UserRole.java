package ru.darvell.rostelecom.oprosservice.model;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "opros_role")
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class UserRole implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String role;

    @Override
    public String getAuthority() {
        return role;
    }

}
