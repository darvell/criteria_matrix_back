package ru.darvell.rostelecom.oprosservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MatrixNotFoundException extends RuntimeException{

    public MatrixNotFoundException(){
        super("Matrix not found");
    }
}
