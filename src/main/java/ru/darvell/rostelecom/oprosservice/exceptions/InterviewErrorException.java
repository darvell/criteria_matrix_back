package ru.darvell.rostelecom.oprosservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InterviewErrorException extends RuntimeException{

    public InterviewErrorException(){
        super("Interview contains error");
    }

    public InterviewErrorException(String message){
        super(message);
    }
}
