package ru.darvell.rostelecom.oprosservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class OprosCloseException extends RuntimeException {

    public OprosCloseException(){
        super("Opros closed");
    }

    public OprosCloseException(String s){
        super(s);
    }
}
