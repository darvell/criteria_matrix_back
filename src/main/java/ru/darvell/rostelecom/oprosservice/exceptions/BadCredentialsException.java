package ru.darvell.rostelecom.oprosservice.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BadCredentialsException extends RuntimeException {

    public BadCredentialsException(String s) {
        super("Cannot auth user "+s);
    }

}
