package ru.darvell.rostelecom.oprosservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class OprosNotFoundException extends RuntimeException {

    public OprosNotFoundException(){
        super("Opros not found");
    }

    public OprosNotFoundException(String s){
        super(s);
    }
}
