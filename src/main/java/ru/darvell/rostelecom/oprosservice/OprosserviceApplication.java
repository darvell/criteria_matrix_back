package ru.darvell.rostelecom.oprosservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableCaching
public class OprosserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OprosserviceApplication.class, args);
        System.out.println(new BCryptPasswordEncoder().encode("test"));
//        $2a$10$SQk1EWJ9HV/qgOQAm4J/f.vNk.0zPgImM4Z.LCgFSP5z3YIA2s5c.
    }

}
